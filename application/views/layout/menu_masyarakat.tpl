<div class="navbar-header">
    <button type="button" class="btn-block btn-drop navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
        <strong>MENU</strong>
    </button>
</div>

<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav">
        <li class="active">
            <a href="{site_url('masyarakat')}">
                <div class="text-center">
                    <i class="fa fa-home fa-3x"></i><br>
                    Beranda
                </div>
            </a>
        </li>
        <li>
            <a href="google-maps.html">
                <div class="text-center">
                    <i class="fa fa-comment fa-3x"></i><br>
                    Keluhan Saya
                </div>
            </a>
        </li>
        <li>
            <a href="google-maps.html">
                <div class="text-center">
                    <i class="fa fa-comments-o fa-3x"></i><br>
                    Daftar Keluhan
                </div>
            </a>
        </li>
        <li>
            <a href="google-maps.html">
                <div class="text-center">
                    <i class="fa fa-bar-chart-o fa-3x"></i><br>
                    Statistik 
                </div>
            </a>
        </li>
        <li>
            <a href="google-maps.html">
                <div class="text-center">
                    <i class="fa fa-exclamation-circle fa-3x"></i><br>
                    Kebijakan
                </div>
            </a>
        </li>
    </ul>
</div><!-- /.navbar-collapse -->