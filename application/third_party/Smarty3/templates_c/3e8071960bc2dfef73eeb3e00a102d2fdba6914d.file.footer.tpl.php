<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-05-31 22:14:21
         compiled from "application\views\layout\footer.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11262554cbd02507542-26846781%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3e8071960bc2dfef73eeb3e00a102d2fdba6914d' => 
    array (
      0 => 'application\\views\\layout\\footer.tpl',
      1 => 1433085258,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11262554cbd02507542-26846781',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_554cbd02578d97_10931334',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_554cbd02578d97_10931334')) {function content_554cbd02578d97_10931334($_smarty_tpl) {?><footer>
    <div class="container">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="footer-widget">
                <h1 class="page-header">Apa itu <strong>E-Com</strong>plaints<strong>?</strong></h1> 
                <span class="divider-hr"></span>
                <div class="row content-widget-footer">
                    <div class="col-sm-4">
                        <div class="icon-footer">
                            <img src="<?php echo base_url();?>
assets/img/icon.png" style="width:110px" alt="">
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <p>E-Com<strong>plaints</strong> adalah sebuah sistem informasi yang berfungsi 
                            menampung dan menyalurkan keluhan masyarakat indonesia dan disampaikan kepihak pemerintah baik pemerintah daerah/kota,
                            pemerintah provinsi maupun pemerintah pusat.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="footer-widget">
                <h1 class="page-header">Fitur dari <strong>E-Com</strong>plaints<strong>?</strong></h1>
                <span class="divider-hr"></span>
                <div class="row content-widget-footer">
                    <div class="col-sm-4">
                        <div class="icon-footer">
                            <i class="fa fa-rocket"></i>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <p>Fitur <strong>E-Com</strong>plaints antara lain: Menampung, menanggapi dan menampilkan data keluhan-keluhan dari masyarakat sesuai dengan tingkatan pemerintahan. Data keluhan terssbut dapat langsung dilihat dari masyarakat yang melakukan keluhan serta dapat juga dilihat berupa data keluhan berupa grafik berdasarkan kategori keluhan</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="footer-widget">
                <h1 class="page-header">Cara Menggunakan <strong>E-Com</strong>plaints<strong>?</strong></h1>
                <span class="divider-hr"></span>
                <div class="row content-widget-footer">
                    <div class="col-sm-4">
                        <div class="icon-footer">
                            <i class="fa fa-money"></i>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <p>Jan jane nek soal duit kie angel panjalukanne, amergi kui ra ono ketentuane sing mutlak. Namung karang manungso kie yo butuh duit kanggo tuku beras. Yo iki themes dihargai $<strong>18</strong> wae yo aku <strong>#rapopo</strong>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php }} ?>
